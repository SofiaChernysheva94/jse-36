package ru.t1.chernysheva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.data.DataSaveJsonFasterXmlRequest;
import ru.t1.chernysheva.tm.dto.request.data.DataSaveJsonJaxBRequest;
import ru.t1.chernysheva.tm.enumerated.Role;

public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-json-jaxb";

    @NotNull
    private static final String DESCRIPTION  = "Save data in json.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataSaveJsonJaxBRequest request = new DataSaveJsonJaxBRequest(getToken());
        getDomainEndpoint().saveDataJsonJaxB(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
