package ru.t1.chernysheva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.data.DataLoadBackupRequest;
import ru.t1.chernysheva.tm.dto.response.data.DataLoadBackupResponse;
import ru.t1.chernysheva.tm.enumerated.Role;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-load";

    @NotNull
    private static final String DESCRIPTION = "Load backup from file.";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final DataLoadBackupRequest request = new DataLoadBackupRequest(getToken());
        getDomainEndpoint().loadDataBackup(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
