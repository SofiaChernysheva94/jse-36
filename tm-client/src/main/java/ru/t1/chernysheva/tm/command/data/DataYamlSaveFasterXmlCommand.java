package ru.t1.chernysheva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.data.DataLoadYamlFasterXmlRequest;
import ru.t1.chernysheva.tm.dto.request.data.DataSaveYamlFasterXmlRequest;
import ru.t1.chernysheva.tm.enumerated.Role;

public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-yaml";

    @NotNull
    private static final String DESCRIPTION  = "Save data in yaml file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE YAML]");
        @NotNull final DataSaveYamlFasterXmlRequest request = new DataSaveYamlFasterXmlRequest(getToken());
        getDomainEndpoint().saveDataYamlFasterXml(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
