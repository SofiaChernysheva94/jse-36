package ru.t1.chernysheva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.endpoint.ISystemEndpoint;
import ru.t1.chernysheva.tm.api.service.ICommandService;
import ru.t1.chernysheva.tm.api.service.IPropertyService;
import ru.t1.chernysheva.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @NotNull
    protected ISystemEndpoint getSystemEndpoint() {
        return serviceLocator.getSystemEndpoint();
    }

}
