package ru.t1.chernysheva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.project.ProjectRemoveByIndexRequest;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-remove-by-index";

    @NotNull
    private final String DESCRIPTION = "Remove project by index.";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(getToken());
        request.setIndex(index);

        getProjectEndpoint().removeProjectByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
