package ru.t1.chernysheva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.task.TaskCompleteByIndexRequest;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-complete-by-index";

    @NotNull
    private final String DESCRIPTION = "Complete task by index.";

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(getToken());
        request.setIndex(index);

        getTaskEndpoint().completeTaskByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
