package ru.t1.chernysheva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.endpoint.IDomainEndpoint;
import ru.t1.chernysheva.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    public AbstractDataCommand() {
    }

    @NotNull
    public IDomainEndpoint getDomainEndpoint() {
        return getServiceLocator().getDomainEndpoint();
    }


}


