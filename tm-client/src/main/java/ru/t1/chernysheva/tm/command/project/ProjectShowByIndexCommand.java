package ru.t1.chernysheva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.project.ProjectShowByIndexRequest;
import ru.t1.chernysheva.tm.model.Project;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-show-by-index";

    @NotNull
    private final String DESCRIPTION = "Show project by index.";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(getToken());
        request.setIndex(index);

        @Nullable final Project project = getProjectEndpoint().showProjectByIndex(request).getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
