package ru.t1.chernysheva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.data.DataLoadBinaryRequest;
import ru.t1.chernysheva.tm.dto.request.data.DataSaveBinaryRequest;
import ru.t1.chernysheva.tm.enumerated.Role;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-bin";

    @NotNull
    private static final String DESCRIPTION  = "Save data to binary file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE BINARY]");
        @NotNull final DataSaveBinaryRequest request = new DataSaveBinaryRequest(getToken());
        getDomainEndpoint().saveDataBinary(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
