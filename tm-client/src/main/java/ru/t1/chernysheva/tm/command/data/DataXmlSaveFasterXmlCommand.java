package ru.t1.chernysheva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.data.DataLoadXmlJaxBRequest;
import ru.t1.chernysheva.tm.dto.request.data.DataSaveXmlFasterXmlRequest;
import ru.t1.chernysheva.tm.enumerated.Role;

public final class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-xml";

    @NotNull
    private static final String DESCRIPTION  = "Save data in xml file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        @NotNull final DataSaveXmlFasterXmlRequest request = new DataSaveXmlFasterXmlRequest(getToken());
        getDomainEndpoint().saveDataXmlFasterXml(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
