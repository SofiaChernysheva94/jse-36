package ru.t1.chernysheva.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.user.UserLoginRequest;
import ru.t1.chernysheva.tm.dto.request.user.UserLogoutRequest;
import ru.t1.chernysheva.tm.dto.request.user.UserProfileRequest;
import ru.t1.chernysheva.tm.dto.response.user.UserLoginResponse;
import ru.t1.chernysheva.tm.dto.response.user.UserLogoutResponse;
import ru.t1.chernysheva.tm.dto.response.user.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAuthEndpoint extends IEndPoint {

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndPoint.newInstance(connectionProvider, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndPoint.newInstance(host, port, NAME, SPACE, PART, IAuthEndpoint.class);
    }


    @NotNull
    UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    );

    @NotNull
    UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    );

    @NotNull
    UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserProfileRequest request);

}

