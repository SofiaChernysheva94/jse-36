package ru.t1.chernysheva.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.AbstractRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserLoginRequest extends AbstractRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    public UserLoginRequest(
            @Nullable String login,
            @Nullable String password
    ) {
        this.login = login;
        this.password = password;
    }

}
