package ru.t1.chernysheva.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public class DataLoadYamlFasterXmlRequest extends AbstractUserRequest {

    public DataLoadYamlFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}
