package ru.t1.chernysheva.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chernysheva.tm.dto.response.user.AbstractUserResponse;
import ru.t1.chernysheva.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public class TaskBindToProjectResponse extends AbstractUserResponse {

    private Task task;

    public TaskBindToProjectResponse(Task task) {
        this.task = task;
    }

}
