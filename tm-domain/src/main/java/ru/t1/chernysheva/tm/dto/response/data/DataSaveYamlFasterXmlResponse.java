package ru.t1.chernysheva.tm.dto.response.data;

import lombok.NoArgsConstructor;
import ru.t1.chernysheva.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public class DataSaveYamlFasterXmlResponse extends AbstractResponse {

}
