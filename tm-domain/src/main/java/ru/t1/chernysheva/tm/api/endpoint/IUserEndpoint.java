package ru.t1.chernysheva.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.user.*;
import ru.t1.chernysheva.tm.dto.response.user.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint extends IEndPoint {

        @NotNull
        String NAME = "UserEndpoint";

        @NotNull
        String PART = NAME + "Service";

        @SneakyThrows
        @WebMethod(exclude = true)
        static IUserEndpoint getInstance() {
                return newInstance(HOST, PORT);
        }

        @SneakyThrows
        @WebMethod(exclude = true)
        static IUserEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
                return IEndPoint.newInstance(connectionProvider, NAME, SPACE, PART, IUserEndpoint.class);
        }

        @SneakyThrows
        @WebMethod(exclude = true)
        static IUserEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
                return IEndPoint.newInstance(host, port, NAME, SPACE, PART, IUserEndpoint.class);
        }

        @NotNull
        @WebMethod
        UserLockResponse lockUser(
                @WebParam(name = REQUEST, partName = REQUEST)
                @NotNull UserLockRequest request
        );

        @NotNull
        @WebMethod
        UserUnlockResponse unlockUser(
                @WebParam(name = REQUEST, partName = REQUEST)
                @NotNull UserUnlockRequest request
        );

        @NotNull
        @WebMethod
        UserRemoveResponse removeUser(
                @WebParam(name = REQUEST, partName = REQUEST)
                @NotNull UserRemoveRequest request
        );

        @NotNull
        @WebMethod
        UserRegistryResponse registryUser(
                @WebParam(name = REQUEST, partName = REQUEST)
                @NotNull UserRegistryRequest request
        );

        @NotNull
        @WebMethod
        UserChangePasswordResponse changeUserPassword(
                @WebParam(name = REQUEST, partName = REQUEST)
                @NotNull UserChangePasswordRequest request
        );

        @NotNull
        @WebMethod
        UserProfileResponse showUserProfile(
                @WebParam(name = REQUEST, partName = REQUEST)
                @NotNull final UserProfileRequest request
        );

        @NotNull
        @WebMethod
        UserUpdateProfileResponse updateUserProfile(
                @WebParam(name = REQUEST, partName = REQUEST)
                @NotNull UserUpdateProfileRequest request
        );

}
