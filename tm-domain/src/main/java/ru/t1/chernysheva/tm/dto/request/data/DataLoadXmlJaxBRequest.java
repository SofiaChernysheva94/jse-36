package ru.t1.chernysheva.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public class DataLoadXmlJaxBRequest extends AbstractUserRequest {

    public DataLoadXmlJaxBRequest(@Nullable final String token) {
        super(token);
    }

}
