package ru.t1.chernysheva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.endpoint.IDomainEndpoint;
import ru.t1.chernysheva.tm.api.service.IServiceLocator;
import ru.t1.chernysheva.tm.dto.request.data.*;
import ru.t1.chernysheva.tm.dto.response.data.*;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.chernysheva.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadBackupResponse loadDataBackup(
            @NotNull final DataLoadBackupRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBackup();
        return new DataLoadBackupResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadBase64Response loadDataBase64(
            @NotNull final DataLoadBase64Request request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBase64();
        return new DataLoadBase64Response();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadBinaryResponse loadDataBinary(
            @NotNull final DataLoadBinaryRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBinary();
        return new DataLoadBinaryResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadJsonFasterXmlResponse loadDataJsonFasterXml(
            @NotNull final DataLoadJsonFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonFasterXml();
        return new DataLoadJsonFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadJsonJaxBResponse loadDataJsonJaxB(
            @NotNull final DataLoadJsonJaxBRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonJaxB();
        return new DataLoadJsonJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadXmlFasterXmlResponse loadDataXmlFasterXml(
            @NotNull final DataLoadXmlFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlFasterXml();
        return new DataLoadXmlFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadXmlJaxBResponse loadDataXmlJaxB(
            @NotNull final DataLoadXmlJaxBRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlJaxB();
        return new DataLoadXmlJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadYamlFasterXmlResponse loadDataYamlFasterXml(
            @NotNull final DataLoadYamlFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataYamlFasterXml();
        return new DataLoadYamlFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveBackupResponse saveDataBackup(
            @NotNull final DataSaveBackupRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBackup();
        return new DataSaveBackupResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveBase64Response saveDataBase64(
            @NotNull final DataSaveBase64Request request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBase64();
        return new DataSaveBase64Response();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveBinaryResponse saveDataBinary(
            @NotNull final DataSaveBinaryRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBinary();
        return new DataSaveBinaryResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveJsonFasterXmlResponse saveDataJsonFasterXml(
            @NotNull final DataSaveJsonFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonFasterXml();
        return new DataSaveJsonFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveJsonJaxBResponse saveDataJsonJaxB(
            @NotNull final DataSaveJsonJaxBRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonJaxB();
        return new DataSaveJsonJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveXmlFasterXmlResponse saveDataXmlFasterXml(
            @NotNull final DataSaveXmlFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlFasterXml();
        return new DataSaveXmlFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveXmlJaxBResponse saveDataXmlJaxB(
            @NotNull final DataSaveXmlJaxBRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlJaxB();
        return new DataSaveXmlJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveYamlFasterXmlResponse saveDataYamlFasterXml(
            @NotNull final DataSaveYamlFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataYamlFasterXml();
        return new DataSaveYamlFasterXmlResponse();
    }

}
