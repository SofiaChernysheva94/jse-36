package ru.t1.chernysheva.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.repository.ISessionRepository;
import ru.t1.chernysheva.tm.api.service.ISessionService;
import ru.t1.chernysheva.tm.model.Session;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository repository) {
        super(repository);
    }

}
