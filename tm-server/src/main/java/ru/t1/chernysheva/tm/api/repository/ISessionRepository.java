package ru.t1.chernysheva.tm.api.repository;

import ru.t1.chernysheva.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

}
