package ru.t1.chernysheva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chernysheva.tm.api.repository.IUserRepository;
import ru.t1.chernysheva.tm.api.service.IPropertyService;
import ru.t1.chernysheva.tm.exception.entity.EntityNotFoundException;
import ru.t1.chernysheva.tm.marker.UnitCategory;
import ru.t1.chernysheva.tm.model.User;
import ru.t1.chernysheva.tm.service.PropertyService;

import static ru.t1.chernysheva.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IUserRepository repository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Before
    public void before() {
        repository.add(USER_TEST);
    }

    @After
    public void after() {
        repository.removeAll(USER_LIST);
    }

    @Test(expected = NullPointerException.class)
    public void add() {
        Assert.assertNull(repository.add(NULL_USER));
        Assert.assertNotNull(repository.add(ADMIN_TEST));
        @Nullable final User user = repository.findOneById(ADMIN_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST, user);
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(repository.add(USER_LIST_ADDED));
        for (final User user : USER_LIST_ADDED)
            Assert.assertEquals(user, repository.findOneById(user.getId()));
    }

    @Test
    public void set() {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_LIST_ADDED);
        emptyRepository.set(USER_LIST);
        Assert.assertEquals(USER_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAll() {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_LIST_ADDED);
        Assert.assertEquals(USER_LIST_ADDED, emptyRepository.findAll());
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertNull(repository.findOneById(null));
        Assert.assertNull(repository.findOneById(NON_EXISTING_USER_ID));
        @Nullable final User user = repository.findOneById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST, user);
    }

    @Test
    public void clear() {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_LIST_ADDED);
        emptyRepository.clear();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test(expected = EntityNotFoundException.class)
    public void remove() {
        Assert.assertNull(repository.remove(null));
        @Nullable final User createdUser = repository.add(ADMIN_TEST);
        @Nullable final User removedUser = repository.remove(createdUser);
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_TEST, removedUser);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeById() {
        Assert.assertNull(repository.removeById(null));
        Assert.assertNull(repository.removeById(NON_EXISTING_USER_ID));
        @Nullable final User createdUser = repository.add(ADMIN_TEST);
        @Nullable final User removedUser = repository.removeById(ADMIN_TEST.getId());
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_TEST, removedUser);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeByIndex() {
        Assert.assertNull(repository.removeByIndex(null));
        @Nullable final User createdUser = repository.add(ADMIN_TEST);
        final int index = repository.findAll().indexOf(createdUser);
        @Nullable final User removedUser = repository.removeByIndex(index);
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_TEST, removedUser);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void getSize() {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        Assert.assertEquals(0, emptyRepository.getSize());
        emptyRepository.add(ADMIN_TEST);
        Assert.assertEquals(1, emptyRepository.getSize());
    }

    @Test
    public void removeAll() {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_LIST);
        emptyRepository.removeAll(USER_LIST);
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test(expected = NullPointerException.class)
    public void findByLogin() {
        Assert.assertNull(repository.findByLogin(null));
        @Nullable final User user = repository.findByLogin(USER_TEST.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST, user);
    }

    @Test(expected = NullPointerException.class)
    public void findByEmail() {
        Assert.assertNull(repository.findByEmail(null));
        @Nullable final User user = repository.findByEmail(USER_TEST.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST, user);
    }

    @Test(expected = NullPointerException.class)
    public void isLoginExists() {
        Assert.assertFalse(repository.isLoginExist(null));
        Assert.assertTrue(repository.isLoginExist(USER_TEST.getLogin()));
    }

    @Test(expected = NullPointerException.class)
    public void isEmailExists() {
        Assert.assertFalse(repository.isEmailExist(null));
        Assert.assertTrue(repository.isEmailExist(USER_TEST.getEmail()));
    }

}
