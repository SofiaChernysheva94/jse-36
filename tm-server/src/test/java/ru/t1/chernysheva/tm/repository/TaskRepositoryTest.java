package ru.t1.chernysheva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chernysheva.tm.api.repository.ITaskRepository;
import ru.t1.chernysheva.tm.comparator.NameComparator;
import ru.t1.chernysheva.tm.exception.entity.EntityNotFoundException;
import ru.t1.chernysheva.tm.marker.UnitCategory;
import ru.t1.chernysheva.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

import static ru.t1.chernysheva.tm.constant.ProjectTestData.USER_PROJECT1;
import static ru.t1.chernysheva.tm.constant.TaskTestData.*;
import static ru.t1.chernysheva.tm.constant.UserTestData.ADMIN_TEST;
import static ru.t1.chernysheva.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @Before
    public void before() {
        repository.add(USER_TASK1);
        repository.add(USER_TASK2);
    }

    @After
    public void after() {
        repository.removeAll(TASK_LIST);
    }

    @Test(expected = NullPointerException.class)
    public void add() {
        Assert.assertNull(repository.add(NULL_TASK));
        Assert.assertNotNull(repository.add(ADMIN_TASK1));
        @Nullable final Task task = repository.findOneById(ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1, task);
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(repository.add(ADMIN_TASK_LIST));
        for (final Task task : ADMIN_TASK_LIST)
            Assert.assertEquals(task, repository.findOneById(task.getId()));
    }

    @Test
    public void addByUserId() {
        Assert.assertNull(repository.add(ADMIN_TEST.getId(), NULL_TASK));
        Assert.assertNull(repository.add(null, ADMIN_TASK1));
        Assert.assertNotNull(repository.add(ADMIN_TEST.getId(), ADMIN_TASK1));
        @Nullable final Task task = repository.findOneById(ADMIN_TEST.getId(), ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1, task);
    }

    @Test
    public void createByUserId() {
        @NotNull final Task task = repository.create(ADMIN_TEST.getId(), ADMIN_TASK1.getName());
        Assert.assertEquals(task, repository.findOneById(ADMIN_TEST.getId(), task.getId()));
        Assert.assertEquals(ADMIN_TASK1.getName(), task.getName());
        Assert.assertEquals(ADMIN_TEST.getId(), task.getUserId());
    }

    @Test
    public void createByUserIdWithDescription() {
        @NotNull final Task task = repository.create(ADMIN_TEST.getId(), ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription());
        Assert.assertEquals(task, repository.findOneById(ADMIN_TEST.getId(), task.getId()));
        Assert.assertEquals(ADMIN_TASK1.getName(), task.getName());
        Assert.assertEquals(ADMIN_TASK1.getDescription(), task.getDescription());
        Assert.assertEquals(ADMIN_TEST.getId(), task.getUserId());
    }

    @Test
    public void set() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK_LIST);
        emptyRepository.set(ADMIN_TASK_LIST);
        Assert.assertEquals(ADMIN_TASK_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAll() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK_LIST);
        Assert.assertEquals(USER_TASK_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        Assert.assertEquals(USER_TASK_LIST, repository.findAll(USER_TEST.getId()));
    }

    @Test
    public void findAllComparator() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK_LIST);
        emptyRepository.add(ADMIN_TASK_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(SORTED_TASK_LIST, emptyRepository.findAll(comparator));
    }

    @Test
    public void findAllComparatorByUserId() {
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(USER_TASK_LIST.stream().sorted(comparator).collect(Collectors.toList()), repository.findAll(USER_TEST.getId(), comparator));
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(USER_TASK1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertFalse(repository.existsById(USER_TEST.getId(), NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId(), USER_TASK1.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertNull(repository.findOneById(null));
        Assert.assertNull(repository.findOneById(NON_EXISTING_TASK_ID));
        @Nullable final Task task = repository.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertNull(repository.findOneById(USER_TEST.getId(), null));
        Assert.assertNull(repository.findOneById(USER_TEST.getId(), NON_EXISTING_TASK_ID));
        @Nullable final Task task = repository.findOneById(USER_TEST.getId(), USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test
    public void findOneByIndexByUserId() {
        Assert.assertNull(repository.findOneByIndex(USER_TEST.getId(), null));
        final int index = repository.findAll().indexOf(USER_TASK1);
        @Nullable final Task task = repository.findOneByIndex(USER_TEST.getId(), index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test
    public void findAllByProjectId() {
        Assert.assertEquals(USER_TASK_LIST, repository.findAllByProjectId(USER_TEST.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void clear() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK_LIST);
        emptyRepository.clear();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void clearByUserId() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK1);
        emptyRepository.add(USER_TASK2);
        emptyRepository.clear(USER_TEST.getId());
        Assert.assertEquals(0, emptyRepository.getSize(USER_TEST.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void remove() {
        Assert.assertNull(repository.remove(null));
        @Nullable final Task createdTask = repository.add(ADMIN_TASK1);
        @Nullable final Task removedTask = repository.remove(createdTask);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByUserId() {
        Assert.assertNull(repository.remove(ADMIN_TEST.getId(), null));
        @Nullable final Task createdTask = repository.add(ADMIN_TASK1);
        Assert.assertNull(repository.remove(null, createdTask));
        @Nullable final Task removedTask = repository.remove(ADMIN_TEST.getId(), createdTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_TASK1.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeById() {
        Assert.assertNull(repository.removeById(null));
        Assert.assertNull(repository.removeById(NON_EXISTING_TASK_ID));
        @Nullable final Task createdTask = repository.add(ADMIN_TASK1);
        @Nullable final Task removedTask = repository.removeById(ADMIN_TASK1.getId());
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertNull(repository.removeById(ADMIN_TEST.getId(), null));
        Assert.assertNull(repository.removeById(ADMIN_TEST.getId(), NON_EXISTING_TASK_ID));
        Assert.assertNull(repository.removeById(ADMIN_TEST.getId(), USER_TASK1.getId()));
        @Nullable final Task createdTask = repository.add(ADMIN_TASK1);
        Assert.assertNull(repository.removeById(null, createdTask.getId()));
        @Nullable final Task removedTask = repository.removeById(ADMIN_TEST.getId(), createdTask.getId());
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_TASK1.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeByIndex() {
        Assert.assertNull(repository.removeByIndex(null));
        @Nullable final Task createdTask = repository.add(ADMIN_TASK1);
        final int index = repository.findAll().indexOf(createdTask);
        @Nullable final Task removedTask = repository.removeByIndex(index);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByIndexByUserId() {
        Assert.assertNull(repository.removeByIndex(ADMIN_TEST.getId(), null));
        @Nullable final Task createdTask = repository.add(ADMIN_TASK1);
        final int index = repository.findAll(ADMIN_TEST.getId()).indexOf(createdTask);
        Assert.assertNull(repository.removeByIndex(null, index));
        @Nullable final Task removedTask = repository.removeByIndex(ADMIN_TEST.getId(), index);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_TASK1.getId()));
    }

    @Test
    public void getSize() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        Assert.assertEquals(0, emptyRepository.getSize());
        emptyRepository.add(ADMIN_TASK1);
        Assert.assertEquals(1, emptyRepository.getSize());
    }

    @Test
    public void getSizeByUserId() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        Assert.assertEquals(0, emptyRepository.getSize(ADMIN_TEST.getId()));
        emptyRepository.add(ADMIN_TASK1);
        emptyRepository.add(USER_TASK1);
        Assert.assertEquals(1, emptyRepository.getSize(ADMIN_TEST.getId()));
    }

    @Test
    public void removeAll() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(TASK_LIST);
        emptyRepository.removeAll(TASK_LIST);
        Assert.assertEquals(0, emptyRepository.getSize());
    }

}
